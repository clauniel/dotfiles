#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return
alias ls='ls --color=auto'
PS1='\e[0;32m[\u\e[0;33m@\h\e[m \e[0;36m\W]\e[0;31m\$ \e[m'


#alias ls='ls --color=auto'
#PS1='[\u@\h \W]\$ '

export QSYS_ROOTDIR="/home/husker/altera/14.1/quartus/sopc_builder/bin"

export ALTERAOCLSDKROOT="/home/husker/altera/14.1/hld"
