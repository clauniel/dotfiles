set nocompatible
filetype off
" Pathogen
call pathogen#infect()
call pathogen#helptags()
 
set statusline=%<\ %n:%f\ %m%r%y%=%-35.(line:\ %l\ of\ %L,\ col:\ %c%V\ (%P)%)
let g:EclimCompletionMethod = 'omnifunc'
"  Nerdtree
autocmd!
autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p
let NERDTreeShowBookmarks=1
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_global_ycm_extra_conf = './.ycm_extra_conf.py'
let NERDTreeChDirMode=0
let NERDTreeQuitOnOpen=0
let NERDTreeMouseMode=2
let NERDTreeShowHidden=1
let NERDTreeIgnore=['\.pyc','\~$','\.swo$','\.swp$','\.git','\.hg','\.svn','\.bzr']
let NERDTreeKeepTreeInNewTab=1
let g:nerdtree_tabs_open_on_gui_startup=0
let g:tex_flavor='latex'

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Bundle 'gmarik/vundle'
Bundle 'Valloric/YouCompleteMe'
Bundle 'derekwyatt/vim-scala'
" Plugin 'bling/vim-airline'
Plugin 'rust-lang/rust.vim'

" let g:airline_powerline_fonts = 1;
" let g:airline_theme = 'base16'

filetype plugin indent on
let g:airline_loaded = 1
set background=dark
colorscheme base16-default
let base16colorspace=256
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set number
set encoding=utf-8
set fileencoding=utf-8
syntax enable

